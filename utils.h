
#ifndef ROBOPRO_TEST_SERVER_UTILS_H
#define ROBOPRO_TEST_SERVER_UTILS_H

double det2x2(double array[2][2]) {
    return array[0][0] * array[1][1] - array[0][1] * array[1][0];
}

double det3x3(double array[3][3]) {
    double ar1[2][2] = {{array[1][1], array[1][2]},
                        {array[2][1], array[2][2]}};
    double ar2[2][2] = {{array[1][0], array[1][2]},
                        {array[2][0], array[2][2]}};
    double ar3[2][2] = {{array[1][0], array[1][1]},
                        {array[2][0], array[2][1]}};

    return array[0][0] * det2x2(ar1) - array[0][1] * det2x2(ar2) + array[0][2] * det2x2(ar3);
}

double det4x4(double array[4][4]) {
    double ar1[3][3] = {{array[1][1], array[1][2], array[1][3]},
                        {array[2][1], array[2][2], array[2][3]},
                        {array[3][1], array[3][2], array[3][3]}};

    double ar2[3][3] = {{array[1][0], array[1][2], array[1][3]},
                        {array[2][0], array[2][2], array[2][3]},
                        {array[3][0], array[3][2], array[3][3]}};

    double ar3[3][3] = {{array[1][0], array[1][1], array[1][3]},
                        {array[2][0], array[2][1], array[2][3]},
                        {array[3][0], array[3][1], array[3][3]}};

    double ar4[3][3] = {{array[1][0], array[1][1], array[1][2]},
                        {array[2][0], array[2][1], array[2][2]},
                        {array[3][0], array[3][1], array[3][2]}};


    return array[0][0] * det3x3(ar1) - array[0][1] * det3x3(ar2) + array[0][2] * det3x3(ar3) -
           array[0][3] * det3x3(ar4);
}


double det5x5(double array[5][5]) {
    double ar1[4][4] = {{array[1][1], array[1][2], array[1][3], array[1][4]},
                        {array[2][1], array[2][2], array[2][3], array[2][4]},
                        {array[3][1], array[3][2], array[3][3], array[3][4]},
                        {array[4][1], array[4][2], array[4][3], array[4][4]}};

    double ar2[4][4] = {{array[1][0], array[1][2], array[1][3], array[1][4]},
                        {array[2][0], array[2][2], array[2][3], array[2][4]},
                        {array[3][0], array[3][2], array[3][3], array[3][4]},
                        {array[4][0], array[4][2], array[4][3], array[4][4]}};

    double ar3[4][4] = {{array[1][0], array[1][1], array[1][3], array[1][4]},
                        {array[2][0], array[2][1], array[2][3], array[2][4]},
                        {array[3][0], array[3][1], array[3][3], array[3][4]},
                        {array[4][0], array[4][1], array[4][3], array[4][4]}};

    double ar4[4][4] = {{array[1][0], array[1][1], array[1][2], array[1][4]},
                        {array[2][0], array[2][1], array[2][2], array[2][4]},
                        {array[3][0], array[3][1], array[3][2], array[3][4]},
                        {array[4][0], array[4][1], array[4][2], array[4][4]}};

    double ar5[4][4] = {{array[1][0], array[1][1], array[1][2], array[1][3]},
                        {array[2][0], array[2][1], array[2][2], array[2][3]},
                        {array[3][0], array[3][1], array[3][2], array[3][3]},
                        {array[4][0], array[4][1], array[4][2], array[4][3]}};


    return array[0][0] * det4x4(ar1) - array[0][1] * det4x4(ar2) + array[0][2] * det4x4(ar3) -
           array[0][3] * det4x4(ar4) + array[0][4] * det4x4(ar5);
}

double det6x6(double array[6][6]) {
    double ar1[5][5] = {{array[1][1], array[1][2], array[1][3], array[1][4], array[1][5]},
                        {array[2][1], array[2][2], array[2][3], array[2][4], array[2][5]},
                        {array[3][1], array[3][2], array[3][3], array[3][4], array[3][5]},
                        {array[4][1], array[4][2], array[4][3], array[4][4], array[4][5]},
                        {array[5][1], array[5][2], array[5][3], array[5][4], array[5][5]}};

    double ar2[5][5] = {{array[1][0], array[1][2], array[1][3], array[1][4], array[1][5]},
                        {array[2][0], array[2][2], array[2][3], array[2][4], array[2][5]},
                        {array[3][0], array[3][2], array[3][3], array[3][4], array[3][5]},
                        {array[4][0], array[4][2], array[4][3], array[4][4], array[4][5]},
                        {array[5][0], array[5][2], array[5][3], array[5][4], array[5][5]}};

    double ar3[5][5] = {{array[1][0], array[1][1], array[1][3], array[1][4], array[1][5]},
                        {array[2][0], array[2][1], array[2][3], array[2][4], array[2][5]},
                        {array[3][0], array[3][1], array[3][3], array[3][4], array[3][5]},
                        {array[4][0], array[4][1], array[4][3], array[4][4], array[4][5]},
                        {array[5][0], array[5][1], array[5][3], array[5][4], array[5][5]}};

    double ar4[5][5] = {{array[1][0], array[1][1], array[1][2], array[1][4], array[1][5]},
                        {array[2][0], array[2][1], array[2][2], array[2][4], array[2][5]},
                        {array[3][0], array[3][1], array[3][2], array[3][4], array[3][5]},
                        {array[4][0], array[4][1], array[4][2], array[4][4], array[4][5]},
                        {array[5][0], array[5][1], array[5][2], array[5][4], array[5][5]}};

    double ar5[5][5] = {{array[1][0], array[1][1], array[1][2], array[1][3], array[1][5]},
                        {array[2][0], array[2][1], array[2][2], array[2][3], array[2][5]},
                        {array[3][0], array[3][1], array[3][2], array[3][3], array[3][5]},
                        {array[4][0], array[4][1], array[4][2], array[4][3], array[4][5]},
                        {array[5][0], array[5][1], array[5][2], array[5][3], array[5][5]}};

    double ar6[5][5] = {{array[1][0], array[1][1], array[1][2], array[1][3], array[1][4]},
                        {array[2][0], array[2][1], array[2][2], array[2][3], array[2][4]},
                        {array[3][0], array[3][1], array[3][2], array[3][3], array[3][4]},
                        {array[4][0], array[4][1], array[4][2], array[4][3], array[4][4]},
                        {array[5][0], array[5][1], array[5][2], array[5][3], array[5][4]}};

    return array[0][0] * det5x5(ar1) - array[0][1] * det5x5(ar2) + array[0][2] * det5x5(ar3) -
           array[0][3] * det5x5(ar4) + array[0][4] * det5x5(ar5) - array[0][5] * det5x5(ar6);
}

#endif //ROBOPRO_TEST_SERVER_UTILS_H
