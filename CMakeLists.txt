cmake_minimum_required(VERSION 3.27)
project(robopro_test_server)

set(CMAKE_CXX_STANDARD 17)
find_package(Threads REQUIRED)

add_executable(robopro_test_server main.cpp
        utils.h)
