#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <list>
#include <numeric>
#include <memory.h>
#include <iostream>
#include "utils.h"

#define PORT_RECEIVE 25000
#define MAX_SIZE_LIST_DETERMINANT 5
#define ROW 6
#define COLUMN 6


void *receiveThread(void *arg) {
    /* нулить не будем потому, что будем заполнять принятыми значениями*/
    double array[ROW][COLUMN];
    /*список последних пяти посчитанных определителей*/
    std::list<double> list_determinant;
    /*сокет на прием по UDP по локалхосту*/
    int *sock = (int *) arg;
    /*для мултиплексора множество, хотя можно и без мультиплексора*/
    fd_set setReceive;
    /*адрес клиента - откуда шлется массив*/
    struct sockaddr_in addressClient;
    socklen_t len = 0;
    /*количество принятых байтов*/
    ssize_t countBytes = 0;
    /*цикл чтения из сокета*/
    for (;;) {
        FD_ZERO(&setReceive);
        FD_SET(*sock, &setReceive);
        if (select(*sock + 1, &setReceive, NULL, NULL, NULL) == -1) {
            std::cout << "Error select receive" << std::endl;
            continue;
        }

        if (!FD_ISSET(*sock, &setReceive))
            continue;
        if ((countBytes = recvfrom(*sock, array,
                                   ROW * COLUMN * sizeof(double), 0,
                                   (struct sockaddr *) &addressClient, &len)) == -1)
            continue;
        std::cout << "----------------------------------------------" << std::endl;
        std::cout << "Receive " << countBytes << " bytes" << std::endl;

        /*печатаем массив*/
        for (int i = 0; i < ROW; i++) {
            for (int j = 0; j < COLUMN; j++)
                std::cout << array[i][j] << " ";
            std::cout << std::endl;
        }

        double determinant = det6x6(array);



        /*если детерминантов меньше 5 дополняем список в начало*/
        if (list_determinant.size() < MAX_SIZE_LIST_DETERMINANT)
            list_determinant.push_front(determinant);
            /*удалим старый определитель и добавим в начало новый*/
        else {
            list_determinant.pop_back();
            list_determinant.push_front(determinant);
        }


        double sum_determinant = 0.0;
        if (list_determinant.size() == MAX_SIZE_LIST_DETERMINANT)
            sum_determinant = std::accumulate(list_determinant.begin(), list_determinant.end(), 0.0);

        std::cout << "Det:" << std::fixed << determinant << std::endl;
        if (list_determinant.size() == MAX_SIZE_LIST_DETERMINANT) {
            std::cout << "Avg. det:" << sum_determinant / MAX_SIZE_LIST_DETERMINANT << std::endl;
            std::cout << "Del. det:" << list_determinant.back() << std::endl;
        } else {
            std::cout << "Avg. det: N/A" << std::endl;
            std::cout << "Del. det: N/A" << std::endl;
        }

        /*здесь печать посчитанных детерминантов, чтоб визуально было видно движение по списку*/
        std::cout << "All the latest determinants:" << std::endl;
        for (const auto &el: list_determinant)
            std::cout << el << "  |  ";

        std::cout << std::endl;


    }//for(;;)
    return NULL;
}


int main(int argc, char **argv) {

    int errorThread = 0;
    int on = 1;
    int socketReceive = 0;
    struct sockaddr_in addressReceive;
    pthread_t threadReceive = 0;
    memset(&addressReceive, 0, sizeof(struct sockaddr_in));

    addressReceive.sin_family = AF_INET;
    addressReceive.sin_port = htons(PORT_RECEIVE);

    if ((socketReceive = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        fprintf(stdout, "Error socket receive\n");
        return -1;
    }
    if (setsockopt(socketReceive, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) == -1) {
        fprintf(stdout, "Error setsockopt socket receive\n");
        return -2;
    }

    if (bind(socketReceive, (struct sockaddr *) &addressReceive, sizeof(struct sockaddr_in)) == -1) {
        fprintf(stdout, "Error bind socket receive\n");
        return -3;
    }


    if ((errorThread = pthread_create(&threadReceive, NULL, receiveThread, &socketReceive)) != 0) {
        fprintf(stdout, "Error pthread_create\n");
        return -4;
    }

    pthread_join(threadReceive, NULL);
    return 0;
}